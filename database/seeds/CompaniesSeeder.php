<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //data faker indonesia
        $faker = Faker::create('id_ID');

        for($i = 0; $i < 10; $i++)
        {
        	DB::table('companies')->insert([
        		'company_name' => $faker->name,
        		'company_address' => $faker->address
        	]);
        }
    }
}
