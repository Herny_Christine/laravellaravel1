<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use App\Models\Pegawai;


class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //insert data to Pegawai
       /* DB::table('pegawai')->insert([
        	'pegawai_nama' => 'Joni',
        	'pegawai_jabatan' => 'General Manager',
        	'pegawai_umur' => 26,
        	'pegawai_alamat' => 'Bandung'
        ]);*/

       /* $pegawai = new Pegawai();
        $pegawai->pegawai_nama = 'Joni';
	    $pegawai->pegawai_jabatan = 'General Manager';
	    $pegawai->pegawai_umur = 26;
	    $pegawai->pegawai_alamat = 'Bandung';
	    $pegawai->save();*/

	    $faker = Faker::create('id_ID');
	    for($i = 1; $i <= 50; $i++){
	    	DB::table('pegawai')->insert([
	    		'pegawai_nama' => $faker->name,
	    		'pegawai_jabatan' => $faker->jobTitle,
	    		'pegawai_umur' => $faker->numberBetween(25,40),
	    		'pegawai_alamat' => $faker->address

	    	]);
	    }

    }
}
