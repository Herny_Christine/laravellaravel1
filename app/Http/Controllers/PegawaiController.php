<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Pegawai;

class PegawaiController extends Controller
{
	/*public function index(){
		// when dealing with a database, just jump right to the Models part in Laravel. Read the documentation for "Eloquent ORM"

		$pegawai = Pegawai::get();

		dd($pegawai);

		//mengambil data dari tabel pegawai
		$pegawai = DB::table('pegawai')->get();

		//mengirim data pegawai ke view index
		return view('index',['pegawai' => $pegawai]);
	}*/

	//Eloquent
	/*public function index(){
		$pegawai = Pegawai::all();
		//dd($pegawai);
		//die();
		return view('pegawai',['pegawai' => $pegawai]);
	}*/

	public function index(){
		$pegawai = DB::table('pegawai')->paginate(10);

		return view('index',['pegawai' => $pegawai]);
	}

	public function formulir(){
		return view('formulir');
	}

	public function proses(Request $request){
		$nama = $request->input('nama');
		$alamat = $request->input('alamat');
		return "Nama: " .$nama. ", Alamat: " .$alamat;
	}

	public function cari(Request $request){
				// mengambil data dari table pegawai sesuai pencarian data
		$pegawai = DB::table('pegawai')
		->where('pegawai_nama','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('index',['pegawai' => $pegawai]);
	}
}