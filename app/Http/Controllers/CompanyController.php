<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller
{
    //
    public function save(Request $req){
    	//print_r($req->input());

    	$company = new Company;
    	$company->company_name = $req->name;
    	$company->company_addr = $req->addr;
    	echo $company->save();
    }
}
