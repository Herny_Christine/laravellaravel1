<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class tests extends Controller
{
    //
    /*public function index(){
    	return ['name' => 'Herny Christine'];
    }
    public function show($id){
    	echo "Hello from controller " . $id;
    }*/

    //pass data from controller to view, second argument as parameter data
  	 public function index(){
    	return view('users', ['name' => 'Herny Christine']);
    }

}
