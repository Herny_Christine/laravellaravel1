<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadController extends Controller
{
    //
    public function store(Request $req)
    {
    	//echo "Hello from controller";
    	$path = $req->file('img')->store('avatars');
    	return ['path' => $path, 'upload' => 'success'];
    }
}
