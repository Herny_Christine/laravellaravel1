<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DosenController extends Controller
{
    //
    public function index(){
    	$nama = "Herny Christine";
    	$pelajaran = ["Chemistry","Physics","Math"];

    	return view('biodata',['nama' => $nama, 'matkul' => $pelajaran]);
    }
}
