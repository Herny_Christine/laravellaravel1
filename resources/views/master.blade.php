<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<header>
		<h2>Blog </h2>
		<nav>
			<a href="/blog">HOME</a>
			|
			<a href="/blog/tentang">TENTANG</a>
			|
			<a href="/blog/kontak">KONTAK</a>
		</nav>
	</header>
	<br/>
	<br/>
	<h3> @yield('Judul halaman')</h3>

	@yield('konten')
	
</body>
</html>