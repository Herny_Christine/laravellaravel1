<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
	<title></title>
</head>
<body>
	<style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin: 5px;
		}
	</style>

	<h3>Data Pegawai</h3>

	<form action="/pegawai/cari" method="GET">
		<input type="text" name="cari" placeholder="Cari pegawai.." value="{{ old('cari') }}">
		<input type="submit" value="CARI">
	</form>
	<a href="/pegawai/tambah"> + Tambah Pegawai Baru </a>

	<br/>
	<br/>

	<table border="1">
		<tr>
			<th>Nama</th>
			<th>Jabatan</th>
			<th>Umur</th>
			<th>Alamat</th>

		</tr>
		@foreach($pegawai as $p)
		<tr>
			<td>{{ $p -> pegawai_nama}}</td>
			<td>{{ $p -> pegawai_jabatan}}</td>
			<td>{{ $p -> pegawai_umur }}</td>
			<td>{{ $p -> pegawai_alamat }}</td>
		</tr>
		@endforeach
	</table>

	Halaman : {{ $pegawai->currentPage() }} <br/>
	Jumlah Data : {{ $pegawai->total() }} <br/>
	Data Per Halaman : {{ $pegawai->perPage() }} <br/>

	<p> {{ $pegawai->links() }} </p>
</body>
</html>