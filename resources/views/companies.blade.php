<!DOCTYPE html>
<html>
<head>
	<title>Company list</title>
</head>
<body>
	<h1>Company List</h1>
	<ul>
		@foreach($companies as $c)
			<li>Company name : {{ $c->company_name }}</li>
		@endforeach
	</ul>
</body>
</html>