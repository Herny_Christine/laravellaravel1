<!DOCTYPE html>
<html>
<head>
	<title>Profile Page</title>
</head>
<body>
	<h2>{{__('profile.welcome')}}</h2>
	<a href="#">{{__('profile.home')}}</a>
	<a href="#">{{__('profile.setting')}}</a>
	<br/>
	<a href="/profile/en">English</a>
	<a href="/profile/id">Indonesia</a>
</body>
</html>