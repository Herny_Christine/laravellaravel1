<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') -page</title>
	<style type="text/css">
		.header: {
			color: green;
		}
		.content: {
			color: red;
		}
	</style>
</head>
<body>
	<div class="header">
		@section('header')
		<h1>Header is common</h1>
		@show
	</div>
	<div class="content">
		@section('content')
		@show
	</div>
</body>
</html>