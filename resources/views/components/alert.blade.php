<div class="alert alert-success">
    <!-- When there is no desire, all things are at peace. - Laozi -->
    <h4 class="alert-title">{{ $title }}</h4>
    <p> {{ $slot }}</p>
</div>