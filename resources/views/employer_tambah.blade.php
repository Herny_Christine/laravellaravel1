<!DOCTYPE html>
<html>
<head>
	 <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Tutorial Laravel #21 : CRUD Eloquent Laravel - www.malasngoding.com</title>
</head>
<body>
	<div class="container">
		<div class="card mt-5">
			<div class="card-header text-center">
				  CRUD Data Pegawai - <strong>TAMBAH DATA</strong> - <a href="https://www.malasngoding.com/category/laravel" target="_blank">www.malasngoding.com</a>
            </div>
            <div class="card-body">
                <a href="/employer" class="btn btn-primary">Kembali</a>
                <br/>
                <br/>

                <form action="/employer/store" method="POST">
                	{{ csrf_field() }}
                	<div class="form-group">
                		<label>Employer name: </label>
                		<input type="text" name="name" placeholder="employer name">

                		@if($errors->has('name'))
                			<div class="text-danger">
                				{{ $errors-> first('name') }}
                			</div>
                		@endif

                	</div>

                	<div class="form-group">
                		<label>Employer Address: </label>
                		<input type="text" name="address" placeholder="employer address">

                		@if($errors -> has('address'))
                			<div class="text-danger">
                				{{ $errors->first('address')}}
                			</div>
                		@endif
                	</div>

                	<div class="form-group">
                		<input type="submit" class="btn btn-success" value="Submit">
                	</div>
                </form>
               </div>
              </div>
             </div>
</body>
</html>