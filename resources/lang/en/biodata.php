<?php
	return[
		"welcome" => "Welcome to our site.",
		"title" => "Please fill the form correctly",
		"profil" => [
			"name" => "Your Name",
			"address" => "Your Address",
			"hobby" => "Your Hobby",
			"job" => "Your Job",
		],
		"button" => "Save",
		"thank" => "Thank you for your contribution",
	];